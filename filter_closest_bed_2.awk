BEGIN{
    OFS="\t";
}
{
    # Default gene name
    gene="_na_";
    accession="_na_"
    
    # First search if "gene=...." annotation is there.
    match($9, /gene=([a-z,A-Z,0-9]+)/, a);
    if(length(a) > 0) 
        gene=a[1];
    
    # Now search for "Name=..."
    match($9, /Name=([a-z,A-Z,0-9,_,\.]+);/, b);
    if(length(b) > 0) 
        accession=b[1];

    print $1,$2,$3,$4,$5,$6,$7,$8,accession,gene; 
} 