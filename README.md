**00a_gather_peak_regions_02.ipynb** 

Purpose

* Gets top 10 summits for each TF, finds the sequences corresponding to them on each genome

* Finds all intergenic regions, and their corresponding upstream and downstream genes (and their accessions)

* Scans all summits for motif hits, and annotates them. (assigns an intergenic region id; upstream and downstream gene names, coords, etc to each hit)

Input: 

* /global/dna/projectdirs/RD/DAPseq/cromwell_pipeout/48sp01/

* .../48sp01_selected_motifs/

* Index_to_species.tsv: indices for all genomes

* TF_Nos_{ECOLI,PSIMIAE}.tsv: Names/numbers of all TFs in the experiment.

* Motif files (48sp01_selected_motifs/{TF_name}.txt)

* Leos orthology file: data/orthos/multidap_orthogroup_overlap_pairs_no_operons.pkl

* References and .gff genome annotations

* Output:

* data/summit_seqs/{DAP_species}/{TF_name}/{genome_name}_summit_seqs.fa: Sequences of all summit ranges (of each DAP peak) for that TF, from that genome.

* data/summit_seqs/{DAP_species}/{TF name}/TF_SUMMIT_SEQS_ALL_GENOMES.TSV: Sequence of each summit range (of each DAP peak) for that TF, from all genomes, concatenated. 

* data/refs/{genome_name}/intergenics_annotated.bed: Intergenic regions, their accessions, and corresponding upstream and downstream genes.

* data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED.bed and .pkl (pandas dataframe): Annotations for fimo hits. Column names:

1. Chromosome/genome 

2. Start coord of the motif hit

3. End coord of the motif hit

4. Fimo score

5. Orientation of the hit

6. Blank 

7. Annotations from fimo

8. Chromosome/genome of Intergenic region the motif hit is on (only intergenic hits are considered)

9. Start coord of the intergenic region

10. End coord of the intergenic region

11. Label of the Intergenic region

12. Type of the upstream gene 

13. Start coord of the upstream gene 

14. End coord of the upstream gene 

15. Orientation of the upstream gene 

16. Accession of the upstream gene  

17. Name of the upstream gene 

18. Type of the downstream gene 

19. Start coord of the downstream gene 

20. End coord of the downstream gene 

21. Orientation of the downstream gene 

22. Accession of the downstream gene  

23. Name of the downstream gene 

24. TF name/id 

25. Hit species/genome
**------ Fields below are in the .pkl pandas dataframe**

26. Target_loc: Possible gene(s) the TF might be regulation based on gene orientation and orthology: upstream, downstream, both, or neither.

27. U_is_ortho: Upstream gene has a Psimiae/Ecoli ortholog 

28. D_is_ortho: Upstream gene has a Psimiae/Ecoli ortholog 

29. Target_is_ortho:  Target gene has a Psimiae/Ecoli ortholog. If upstream and downstream genes are in -/+ orientation, respectively, only one of them need to be an ortholog for this field to be set to True.
**------ Fields below are added by 00b_assign_peaks.ipynb**

30.  Peak_id: Id of the DAP peak the motif hit was found in

31. Peak enrichment: peak fold change from macs2

32. Peak start: Peak start coord

33. Peak end: Peak end coord

34. Indexcol: not important. Used for numbering.

35. **------ Fields below are added by 01_fimo_hits_analysis_02.ipynb**

36. Redundant: True if this hit is on the antisense strand, and overlaps by >90% with another hit by the same TF on the sense strand. False otherwise.    

37. Midpoint: midpoint of the hit     

38. Length: length of the hit     

39. Prefferred_target_loc: The upstream or the downstream gene with the correct orientation and orthology matching. If both up/downstream genes have the correct orientation and orthology, pick the closer gene.     

40.  Dist_to_pref_tgt: Distance from the midpoint to the gene start coord.

**00b_assign_peaks.ipynb**

Purpose:

* Assigns each DAP peaks to corresponding motif hits.

Input:

* orthos/multidap/*summits_blist_top10.narrowPeak 

* data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED.pkl: dataframe

Output:

* data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED_W_PEAKS.pkl: dataframe

**01_fimo_hits_analysis_02.ipynb**

Purpose:

* Groups occurrences by TF, intergenic region & peak id; then finds overlapping hits (>90%) on opposite strands; marks the one on the antisense strand as redundant.

Input:

* data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED_W_PEAKS.pkl: dataframe

Output:

* Updates data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED_W_PEAKS.pkl 

**02_run_pfam_on_tf_seqs.ipynb**

Purpose:

* Annotate each TF with its pfam hits, then cluster the TFs by their annotations.

Input

* data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED_W_PEAKS.pkl 

* /data/tf_sequences/{DAP_SPECIES}_tf_accessions.tsv

* .faa references

* Pfam database for hmmer

Output:

* A seaborn clustermap for all TFs/pfam hits (except singletons) inside the notebook.

**03_peaks_analysis_01.ipynb**

Purpose:

* Plots various statistics: motif scores, intervals, and correlations.

Input:

* data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED_W_PEAKS.pkl 

Output:

* Plots inside the notebook.

**viz_promoters.ipynb**

Purpose:

* Visualizes binding sites of a TF.

Input:

* data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED_W_PEAKS.pkl 

Output:

* Plots inside the notebook.

**viz_stats_01.ipynb**

Purpose:

* Plots some distributions such as #of promoters pef TF, etc; as well as heatmaps (distances between hits for each TF, etc)  .

Input:

* data/{DAP_sepecies}_RESULTS/ALL_FIMO_HITS_ANNOTATED_W_PEAKS.pkl 

Output:

* Plots inside the notebook.
