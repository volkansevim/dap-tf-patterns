BEGIN{
    OFS="\t";
}
{
    # Default gene name
    gene="Gene";
    
    # First search if "gene=...." annotation is there.
    match($9, /gene=([a-z,A-Z,0-9]+)/, a);
    #print "a:", length(a)
    if(length(a)>0) 
        gene=a[1];
    
    # If "gene=..." is not found, search for "Name=..."
    else 
        match($9, /Name=([a-z,A-Z,0-9,_,\.]+);/, a);
        if(length(a)>0) 
            gene=a[1];

    print $1,$2,$3,$4,$5,$6,$7,$8,gene; 
} 