#!/bin/bash 

set -e
module load samtools
blue=$(tput setaf 4)
red=$(tput setaf 1)
normal=$(tput sgr0)

PATH_TO_SCRIPTS="/global/homes/v/vsevim/scratch/ML/grammar/nb/"
PATH_TO_REFS="/global/homes/v/vsevim/scratch/ML/grammar/data/refs/"

while read -r SPECIES; do
    REF_PATH="$PATH_TO_REFS/$SPECIES/"
    REF_GFF="$SPECIES.gff"
    REF_FASTA="$SPECIES.fasta"
    REF_SORTED_GFF="$SPECIES.sorted.gff"
    UNSORTED_REF_INDEX="$SPECIES.fasta.fai"
    REF_INDEX="$SPECIES.sorted.fasta.fai"
    FILTERED_GFF_FILE="$SPECIES""_filt.gff"
    SORTED_GENES_GFF="filtered_annotations.sorted.gff"
    INTERGENICS_BED="intergenics.bed"
    INTERGENIC_NAME_SEPERATOR=":"

    # Sort the ref
    cd "$REF_PATH" 

    if [[ ! -f "$REF_GFF" ]]; then
        printf  "${red}$SPECIES skipping...${normal}\n"
        continue
    fi
    
    printf "${blue} * $SPECIES * ${normal}: "
    echo "Sorting $REF_GFF"
    bedtools sort -i "$REF_GFF" > "$REF_SORTED_GFF"

    echo "Indexing..."
    samtools faidx "$REF_FASTA" 

    echo "Sorting $UNSORTED_REF_INDEX"
    sort -k1,1 "$UNSORTED_REF_INDEX" > "$REF_INDEX"

    # Sort the reference and the motif hits
    echo "Sorting $FILTERED_GFF_FILE into $SORTED_GENES_GFF"
    bedtools sort -i "$FILTERED_GFF_FILE" > "$SORTED_GENES_GFF"
    
    # Get intergenic regions
    echo "Getting intergenic regions, writing into dummy.bed"
    echo "bedtools complement -i $SORTED_GENES_GFF -g $REF_INDEX > dummy.bed"
    bedtools complement \
        -i "$SORTED_GENES_GFF" \
        -g "$REF_INDEX" |
        awk -v sep="$INTERGENIC_NAME_SEPERATOR" \
            '{printf "%s\t%s%s%05d\n",$0,$1,sep,i++; }' > \
            "dummy.bed"

    echo "Sorting intergenic regions, writing into $INTERGENICS_BED"
    bedtools sort -i dummy.bed > "$INTERGENICS_BED"
    num_intergenic_seqs=`wc -l "$INTERGENICS_BED"`
    echo "#intergenic regions=$num_intergenic_seqs"

    # Find the upstream gene of each intergenic region
    echo "Finding upstream genes of each intergenic seq, writing into dummy.bed..."
    bedtools closest \
        -D ref -fu -t first \
        -a "$INTERGENICS_BED" \
        -b "$SORTED_GENES_GFF" |\
            cut -f 1-4,7-9,11,13 > "dummy.bed" 
    
    echo "Extracting gene names from the output..."
    echo "(Running $PATH_TO_SCRIPTS/filter_closest_bed.awk)"
    awk -f "$PATH_TO_SCRIPTS/filter_closest_bed.awk" \
        "dummy.bed" > "dummy2.bed"
    
    echo "Sorting, writing into upstreams.bed..."
    bedtools sort -i "dummy2.bed" > "upstreams.bed"

    # Find the downstream gene of each intergenic region
    echo "Finding upstream genes of each intergenic seq, writing into dummy.bed..."
    bedtools closest \
        -D ref -fd -t first \
        -a "$INTERGENICS_BED" \
        -b "$SORTED_GENES_GFF" |\
            cut -f 1-4,7-9,11,13 > "dummy.bed"
    

    echo "Extracting gene names from the output..."
    awk -f "$PATH_TO_SCRIPTS/filter_closest_bed.awk" \
        "dummy.bed" > "dummy2.bed"
    
    echo "Sorting, writing into downstreams.bed..."
    bedtools sort -i "dummy2.bed" > "downstreams.bed"

    echo "Merging, creating intergenics_and_neighbors.bed"
    paste "upstreams.bed" "downstreams.bed" | \
        cut -f1-9,14-18 > \
        "intergenics_and_neighbors.bed"
    
    rm dummy*
    printf "*******\n"
done < "species_order.txt"