Escherichia_coli_MG1655
Escherichia_coli_BW25113
Shigella_flexneri_ATCC700930
Escherichia_fergusonii_ATCC35469
Salmonella_enterica_LT2
Citrobacter_koseri_ATCCBAA-895
Klebsiella_psneumoniae_ATCC700721
Klebsiella_oxytoca_M5al
Enterobacter_cloacae_ATCC13047
Providencia_stuartii_ATCC33672
Haemophilus_influenzae_ATCC51907
Aliivibrio_fischeri_ATCC700601
Aeromonas_hydrophila_ATCC7966
Shewanella_sp_ANA-3
Shewanella_oneidensis_MR-1
Shewanella_loihica_PV-4
Shewanella_amazonensis_SB2B
Kangiella_aquimarina_DSM16071
Saccharophagus_degradans_ATCC43961
Marinobacter_hydrocarbonoclasticus_ATCC700491
Marinobacter_adhaerens_HP15
Pseudomonas_aeruginosa_PAO1
Pseudomonas_stutzeri_RCH2
Pseudomonas_putida_KT2440
Pseudomonas_simiae_WCS417
Pseudomonas_fluorescens_FW300-N1B4
Pseudomonas_fluorescens_GW456-L13
Pseudomonas_fluorescens_FW300-N2E3
Pseudomonas_fluorescens_FW300-N2C3
Pseudomonas_fluorescens_FW300-N2E2
Dyella_japonica_UNC79MFTsu3.2
Burkholderia_bryophila_376MFSha3.1
Paraburkholderia_phytofirmans_PsJN
Ralstonia_sp_UNC404CL21Col
Cupriavidus_basilensis_4G11
Herbaspirillum_seropedicae_SmR1
Acidovorax_sp_GW101-3H11
Phaeobacter_inhibens_DSM17395
Dinoroseobacter_shibae_DFL-12-DSM16493
Sinorhizobium_meliloti_1021
Brevundimonas_sp_GW460-12-10-14-LB2
Azospirillum_brasilense_Sp245
Sphingomonas_koreensis_JSS26
Sphingobium_sp_GW456-12-10-14-TSB1
Pontibacter_actiniarum_KMM-6156-DSM19842
Echinicola_vietnamensis_KMM-6221-DSM17526
Sphingobacteriaceae_sp_GW460-11-11-14-LB5
Bacillus_subtilis_168
